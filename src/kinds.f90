module kinds
  
  use iso_fortran_env, only: int64, real64

  implicit none
  private

  public :: i64
  public :: f64
  integer, parameter :: i64 = int64, f64 = real64

end module kinds
