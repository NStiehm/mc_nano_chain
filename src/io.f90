module io

  use kinds, only: i64, f64

  implicit none
  private

  public :: construct_output_filename
  public :: write_to_file

  contains

    pure function construct_output_filename(N, mu) result(output_filename)

      integer(i64), intent(in) :: N
      real(f64), intent(in) :: mu
      
      character(len=100) :: output_filename

      character(len=5) :: mu_str, N_str

      write(mu_str, "(i5.5)") int(mu)
      write(N_str, "(i5.5)") N


      output_filename = "MC_nano_N_" // N_str // "_mu_" // mu_str // ".txt"

    end function construct_output_filename

    subroutine write_to_file(E, L, L_z, N, output_filename)
      integer(i64), intent(in) :: N
      real(f64), dimension(1:N), intent(in) :: E, L, L_z
      character(len=*), intent(in) :: output_filename
      
      integer(i64) :: i, output_file

      open(file = output_filename, newunit = output_file)

      write(output_file, *) "E in E0, L in L0, L_z in L0"

      do i = 1, N
       write(output_file,"(f12.8,a,f12.8,a,f12.8)") E(i), ",", L(i), ",", L_z(i) 
      end do

    end subroutine write_to_file

end module io
