module constants

  use kinds, only: f64, i64

  implicit none
  private

  public :: N_PHI
  public :: CHAIN_LENGTH
  public :: PHI
  public :: COS_PHI
  public :: SIN_PHI

  integer(i64), parameter :: N_PHI = 8
  !! number of discrete angle positions of the individual chain parts
  integer(i64), parameter :: CHAIN_LENGTH = 200
  !! number of individual chain parts
  real(f64), parameter :: PI = 4.0 * atan(1.0)

  integer(i64) :: i

  real(f64), dimension(-N_PHI:N_PHI), parameter :: PHI = [ (real(i) * PI / (N_PHI + 1), i = -N_PHI, N_PHI) ]
  !! angles are chosen such that only < 180 degrees are possible
  !! (the chain can't "fold back" on itself
  real(f64), dimension(-N_PHI:N_PHI), parameter :: COS_PHI = cos(PHI)
  real(f64), dimension(-N_PHI:N_PHI), parameter :: SIN_PHI = sin(PHI)

end module constants
