module mc_routines

  use kinds, only: i64, f64
  use constants, only: CHAIN_LENGTH, COS_PHI, SIN_PHI, N_PHI
  
  implicit none
  private

  public :: mc_sweep
  public :: mc_initialize

  contains

    subroutine mc_sweep(sweep_counter, chain, E, L, L_z, mu)
      integer(i64) :: i, sweep_counter, random_index, random_phi
      integer(i64), dimension(:), intent(inout) :: chain
      real(f64), dimension(:), intent(inout) :: E, L, L_z
      real(f64), intent(in) :: mu

      real(f64), dimension(1:3) :: random
      real(f64) :: E_old, E_new

      do i = 1,CHAIN_LENGTH

        ! get 3 random numbers
        call random_number(random)
        ! generate a random integer between 1 and CHAIN_LENGTH
        random_index = int( random(1) * real(CHAIN_LENGTH) ) + 1
        ! generate a random integer between -N_PHI and N_PHI
        random_phi = int( random(2) * real(2*N_PHI + 1) ) - N_PHI

        ! calculate the energies of the saved phi state and
        ! the newly random generated phi state
        E_old = chain_part_energy(chain(random_index), mu)
        E_new = chain_part_energy(random_phi, mu)

        ! perform metropolis method
        if ( E_new < E_old ) then
          chain(random_index) = random_phi
        elseif ( random(3) < exp(-(E_new - E_old)) ) then
          chain(random_index) = random_phi
        end if

      end do

      ! calculate total chain energy and length
      E(sweep_counter) = sum( chain_part_energy(chain, mu) )
      L(sweep_counter) = sqrt( ( sum( SIN_PHI(chain) ) )**2 + ( sum( COS_PHI(chain) ) )**2 ) / real(CHAIN_LENGTH)
      L_z(sweep_counter) = sum( COS_PHI(chain) ) / real(CHAIN_LENGTH)


    end subroutine mc_sweep

    subroutine mc_initialize(chain, E, L, L_z)
      integer(i64), dimension(:), intent(inout) :: chain
      real(f64), dimension(:), intent(inout) :: E, L, L_z

      chain = 0

      E = 0.0
      L = 0.0
      L_z = 0.0

    end subroutine mc_initialize

    pure elemental function chain_part_energy(phi_index, mu) result(energy)
      integer(i64), intent(in) :: phi_index
      real(f64), intent(in) :: mu
      real(f64) :: energy

      energy = mu / real(CHAIN_LENGTH) * ( 1 - COS_PHI(phi_index) )

    end function chain_part_energy
    

end module mc_routines
