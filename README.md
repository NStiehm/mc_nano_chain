# MC_nano_chain
Monte Carlo simulation of a nano chain. Created for a course on Monte Carlo simulation at TU Ilmenau.

# Build
The simulation is written in Fortran and uses the [fortran package manager](https://fpm.fortran-lang.org/index.html) as a build system. Assuming you have `fpm` installed and on your `PATH`, type

`fpm build --release`

to build the executable for the simulation. A single run which requires manual entry of the number of sweeps and the mu parameter can be executed by running

`fpm run --release`

To perform a sequence of automated runs you can either write your own shell script or utilize the ion shell script at `runs/mu_parameter_scan/parameter_scan.ion` if you have [ion](https://gitlab.redox-os.org/redox-os/ion) installed.

To build the commentary pdf file you need to have `groff` installed and execute the compile script `commentary/compile`.

A sample evaluation script for use with [GNU Octave](https://www.gnu.org/software/octave/index) is located at `runs/mu_parameter_scan/parameter_scan_evaluation.m`
