program main

  use iso_fortran_env, only: compiler_version
  use kinds, only: i64, f64
  use constants, only: CHAIN_LENGTH
  use io, only: construct_output_filename, write_to_file
  use mc_routines, only: mc_sweep, mc_initialize

  implicit none

  integer(i64) :: i
  !! counter

  integer(i64) :: N
  !! number of Monte Carlo Sweeps
  real(f64) :: mu
  !! mass / inverse temperature scaling
  character(len=100) :: output_filename
  !! filename for saving E, L, L_z per sweep

  integer(i64), dimension(1:CHAIN_LENGTH) :: chain
  !! internal representation of the chain as indices into the discrete
  !! angle positions
  real(f64), dimension(:), allocatable :: E, L, L_z
  !! Energy, end to end distance and z distance


  ! initialize the random number generator
  call random_seed()


 ! Ask the user some questions about the run
  write(*,"(a, /, a, 2/)") "Monte Carlo nano chain simulation, compiled with:", compiler_version()

  write(*,*) "How many sweeps do you want to perform? "

  read(*,*) N

  write(*,*) "Please enter a value for mu: "

  read(*,*) mu

  ! Echo back the decisions
  write(*,"(a, i5, a, f5.2)") "Running simulation with N = ", N, ", mu = ", mu

  output_filename = construct_output_filename(N, mu)

  write(*,*) "Output will be written to file: ", output_filename


  ! setup
  allocate(E(1:N), L(1:N), L_z(1:N))
  call mc_initialize(chain, E, L, L_z)


  ! run the simulation
  do i = 1, N
    call mc_sweep(i, chain, E, L, L_z, mu)
  end do
  
  ! write results to disk
  call write_to_file(E, L, L_z, N, output_filename)

end program main
