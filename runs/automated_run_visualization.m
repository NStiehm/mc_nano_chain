clear, clc, close all

data = csvread("MC_nano_N_00200_mu_02_250.txt", 1, 0);

subplot(3,1,1)
plot(data(:,1))
ylabel(" E in E_0 ")
xlabel(" MC sweep ")

subplot(3,1,2)
plot(data(:,2))
ylabel(" L in L_0 ")
xlabel(" MC sweep ")

subplot(3,1,3)
plot(data(:,3))
ylabel(" L_z in L_0 ")
xlabel(" MC sweep ")