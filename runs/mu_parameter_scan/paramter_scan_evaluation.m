files = { ...
"MC_nano_N_00500_mu_00001.txt", 1
"MC_nano_N_00500_mu_00002.txt", 2
"MC_nano_N_00500_mu_00005.txt", 5
"MC_nano_N_00500_mu_00010.txt", 10
"MC_nano_N_00500_mu_00020.txt", 20
"MC_nano_N_00500_mu_00050.txt", 50
"MC_nano_N_00500_mu_00100.txt", 100
"MC_nano_N_00500_mu_00200.txt", 200
"MC_nano_N_00500_mu_00300.txt", 300
"MC_nano_N_00500_mu_00400.txt", 400
"MC_nano_N_00500_mu_00500.txt", 500 };

N = size(files)(1);

mu = zeros(N,1);
mean_E = zeros(N,1);
mean_L = zeros(N,1);
mean_L_z = zeros(N,1);
C = zeros(N,1);


for n = 1:N
  file = files{n,1};
  mu(n) = files{n,2};
  
  data = csvread(file, 1, 0);
  % roughly the first 20 sweeps are
  % equilibration
  E = data(20:end, 1);
  L = data(20:end, 2);
  L_z = data(20:end, 3);
  
  mean_E(n) = mean(E);
  mean_L(n) = mean(L);
  mean_L_z(n) = mean(L_z);
  
  C(n) = mean(E.^2) - mean_E(n)^2;
end

subplot(2,2,1)
title("Energy")
plot(mu, mean_E, "ko", "MarkerSize", 8)
xlabel("\mu")
ylabel("E in E_0")
grid on

subplot(2,2,2)
title("End-to-End Length")
plot(mu, mean_L, "ko", "MarkerSize", 8)
xlabel("\mu")
ylabel("L in L_0")
grid on


subplot(2,2,3)
title("z Length")
plot(mu, mean_L_z, "ko", "MarkerSize", 8)
xlabel("\mu")
ylabel("L_z in L_0")
grid on

subplot(2,2,4)
title("Specific Heat Capacity")
plot(mu, C, "ko", "MarkerSize", 8)
xlabel("\mu")
ylabel("C in E_0^2")
grid on
